import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { Button } from "../../components/button";
import {
  FormInputWrapper,
  FormLabel,
  FormInput,
  FormTitle,
  FormError,
  CustomForm,
  FormTextArea,
  FormButtonWrapper,
} from "../../components/form";
import { addBook, getBook, updateBook } from "../../services/modules/books";
import { updateBookAction } from "../../store/books/actions";
import { BooksState } from "../../store/books/types";
import { sleep } from "../../utils/sleep";

const BooksSchema = Yup.object().shape({
  title: Yup.string().required("**Required Field!**"),
  description: Yup.string().required("**Required Field!**"),
});

export const BooksForm = () => {
  const [disabled, setDisabled] = useState(false);
  const [selectedBook, setSelectedBook] = useState<BooksState>({
    title: "",
    description: "",
    user: "",
    id: "",
  });
  const dispatch = useDispatch();
  const history = useHistory();
  const { id }: { id?: string } = useParams();

  useEffect(() => {
    async function fetchAndSetBooks() {
      if (id) {
        const response = await getBook(id);
        if (response.ok) {
          const json = response.json() as Promise<BooksState>;
          json.then((book) =>
            setSelectedBook({
              title: book.title,
              description: book.description,
              user: book.user,
              id: book.id,
            })
          );
        } else {
          history.push("/main");
        }
      }
    }

    fetchAndSetBooks();
  }, [dispatch, id, history]);

  return (
    <Formik
      enableReinitialize
      initialValues={selectedBook}
      validationSchema={BooksSchema}
      onSubmit={async (values) => {
        setDisabled(true);

        if (selectedBook.user) {
          toast.error("Book already rented!");
          setDisabled(false);
          return null;
        }

        toast.info("Submitting...");

        if (selectedBook.id) {
          await updateBook({
            title: values.title,
            description: values.description,
            id,
          });
        } else {
          await addBook({
            title: values.title,
            description: values.description,
          });
        }

        return sleep(2500).then(async () => {
          setDisabled(false);
          if (id) {
            dispatch(
              updateBookAction({
                title: values.title,
                description: values.description,
                id,
              })
            );
          }
          history.push("/main");
        });
      }}
    >
      {({ errors, touched }) => (
        <CustomForm autoComplete="off">
          <FormTitle> {selectedBook.id ? "Edit Book" : "Add Book"} </FormTitle>
          <FormInputWrapper>
            <FormLabel htmlFor="book-title">Title: </FormLabel>
            <FormInput placeholder="Title" name="title" id="book-title" />
            {errors.title && touched.title ? (
              <FormError>{errors.title}</FormError>
            ) : null}
          </FormInputWrapper>
          <FormInputWrapper>
            <FormLabel htmlFor="book-description">Description: </FormLabel>
            <FormTextArea
              placeholder="Description"
              name="description"
              id="book-description"
              component="textarea"
            />
            {errors.description && touched.description ? (
              <FormError>{errors.description}</FormError>
            ) : null}
          </FormInputWrapper>

          <FormButtonWrapper>
            <Button
              margin="1em 2em 0 0"
              type="button"
              disabled={disabled}
              onClick={() => history.push("/main")}
            >
              BACK
            </Button>

            <Button margin="1em 0 0 0" type="submit" disabled={disabled}>
              SUBMIT
            </Button>
          </FormButtonWrapper>
        </CustomForm>
      )}
    </Formik>
  );
};
