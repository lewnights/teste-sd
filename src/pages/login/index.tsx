import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { Formik } from "formik";
import * as Yup from "yup";
import { toast } from "react-toastify";
import { signIn } from "../../store/auth/actions";
import {
  FormWrapper,
  CustomForm,
  FormInputWrapper,
  FormLabel,
  FormInput,
  FormTitle,
  FormError,
} from "../../components/form";
import { Button } from "../../components/button";
import { LoginProps } from "../../services/types";
import { getUser } from "../../services/modules/user";
import { sleep } from "../../utils/sleep";

const LoginSchema = Yup.object().shape({
  name: Yup.string().required("**Required Field!**"),
  password: Yup.string().required("**Required Field!**"),
});

export const Login = () => {
  const dispatch = useDispatch();
  const [disabled, setDisabled] = useState(false);

  const initialValues: LoginProps = { name: "", password: "" };

  return (
    <FormWrapper>
      <Formik
        initialValues={initialValues}
        validationSchema={LoginSchema}
        onSubmit={async (values) => {
          setDisabled(true);

          toast.info("Submitting...");

          return sleep(2500).then(async () => {
            const [user] = await getUser({
              name: values.name.trim(),
              password: values.password,
            });

            if (user?.id) {
              dispatch(signIn({ id: user.id, name: user.name.trim() }));
            } else {
              toast.error("Oops! Try checking your credentials!");
            }

            setDisabled(false);
          });
        }}
      >
        {({ errors, touched }) => (
          <CustomForm autoComplete="off">
            <FormTitle> Rent Books! </FormTitle>
            <FormInputWrapper>
              <FormLabel htmlFor="login-name">Name: </FormLabel>
              <FormInput placeholder="Username" name="name" id="login-name" />
              {errors.name && touched.name ? (
                <FormError>{errors.name}</FormError>
              ) : null}
            </FormInputWrapper>
            <FormInputWrapper>
              <FormLabel htmlFor="login-password">Password: </FormLabel>
              <FormInput
                placeholder="Password"
                name="password"
                id="login-password"
                type="password"
              />
              {errors.password && touched.password ? (
                <FormError>{errors.password}</FormError>
              ) : null}
            </FormInputWrapper>

            <Button margin="1em 0 0 0" type="submit" disabled={disabled}>
              LOGIN
            </Button>
          </CustomForm>
        )}
      </Formik>
    </FormWrapper>
  );
};
