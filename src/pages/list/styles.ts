import styled, { css } from "@xstyled/styled-components";
import { Button } from "../../components/button";

const ListSvgStyle = css`
  svg {
    width: 20px;
    height: 20px;
    cursor: pointer;
    color: violet-600;

    &:not(:last-of-type) {
      margin-right: 2;
    }
  }
`;

export const ListWrapper = styled.ul`
  margin: 0 auto;

  & > ${Button} {
    margin-left: auto;
    margin-bottom: 5;
  }
`;

export const ListItem = styled.li`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #f8f7fc;

  border-radius: 10px 10px 0 0;
  margin: 2 0 0 0;
  padding: 0 4;
`;

export const ListItemTitle = styled.p`
  font-size: 0.8em;
  color: violet-600;
  font-weight: bold;

  max-width: 60%;
`;

interface ListOpenProps extends React.HTMLAttributes<HTMLDivElement> {
  open?: boolean;
}

export const ListItemCaret = styled.div<ListOpenProps>`
  transition: transform 0.4s ease;

  ${ListSvgStyle};

  ${(p) =>
    p.open &&
    css`
      transform: rotateZ(-180deg);
    `}
`;

export const ListItemInformations = styled.div<ListOpenProps>`
  width: 100%;
  max-height: 0;
  height: 100%;
  opacity: 0;

  transition: 0.5s ease-in-out opacity, 0.4s ease max-height;
  border-radius: 0 0 10px 10px;
  pointer-events: none;

  ${(p) =>
    p.open &&
    css`
      max-height: 500px;
      opacity: 1;
      background-color: #f8f7fc;
      pointer-events: all;
    `};
`;

export const ListItemInformationDescription = styled.p`
  color: gray-500;
  font-size: 0.8em;
  pointer-events: none;
  max-width: 95%;
  margin: 0 auto;
`;

export const ListItemRented = styled.p`
  color: red-500;
  text-align: center;
  font-size: 1em;
  font-weight: bold;
  margin-top: 4;
  padding-bottom: 4;
`;

export const ListItemIcons = styled.ul`
  display: flex;
  justify-content: space-between;
  align-items: center;
  max-width: 50px;
  margin: 4 4 0 auto;
  padding-bottom: 4;

  ${ListSvgStyle};
`;
