import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { AiFillDownCircle, AiTwotoneEdit, AiFillDelete } from "react-icons/ai";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import { deleteBook, updateBook } from "../../services/modules/books";
import { removeBookAction, updateBookAction } from "../../store/books/actions";
import { BooksState } from "../../store/books/types";

import {
  ListItem,
  ListItemTitle,
  ListItemIcons,
  ListItemCaret,
  ListItemInformations,
  ListItemInformationDescription,
  ListItemRented,
} from "./styles";
import { Button } from "../../components/button";
import { StoreInterface } from "../../store/store";

interface ListBookItemProps {
  book: BooksState;
}

export const ListBookItem = ({ book }: ListBookItemProps) => {
  const [open, setOpen] = useState(false);
  const dispatch = useDispatch();
  const user = useSelector((state: StoreInterface) => state.auth.id);

  const history = useHistory();

  const removeBook = async (bookId: string) => {
    await deleteBook({ id: bookId });
    toast.info("Removed book sucessfully!");
    dispatch(removeBookAction(bookId));
  };

  const rentBook = async () => {
    const updatedBook = await updateBook({ ...book, user });
    dispatch(updateBookAction(updatedBook));
  };

  return (
    <>
      <ListItem key={book.id}>
        <ListItemTitle>{book.title}</ListItemTitle>
        {!book.user && (
          <Button margin="0 5px 0 auto" onClick={() => rentBook()}>
            RENT
          </Button>
        )}
        <ListItemCaret open={open}>
          <AiFillDownCircle onClick={() => setOpen(!open)} />
        </ListItemCaret>
      </ListItem>
      <ListItemInformations open={open}>
        <ListItemInformationDescription>
          {book.description}
        </ListItemInformationDescription>
        {book.user ? (
          book.user === user ? (
            <ListItemRented>You have already rented this!</ListItemRented>
          ) : (
            <ListItemRented>Other user has already rented this!</ListItemRented>
          )
        ) : (
          <ListItemIcons>
            <AiTwotoneEdit onClick={() => history.push(`/books/${book.id}`)} />
            <AiFillDelete onClick={() => removeBook(book.id)} />
          </ListItemIcons>
        )}
      </ListItemInformations>
    </>
  );
};
