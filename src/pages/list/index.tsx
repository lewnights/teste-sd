import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Formik } from "formik";
import { toast } from "react-toastify";
import { filterBooks, getAllBooks } from "../../services/modules/books";
import { setBooksAction } from "../../store/books/actions";
import { StoreInterface } from "../../store/store";
import { ListWrapper } from "./styles";
import { Button } from "../../components/button";
import { ListBookItem } from "./list-book-item";
import { SearchForm, FormInput, FormInputWrapper } from "../../components/form";
import { sleep } from "../../utils/sleep";

interface SearchProps {
  query: "";
}

export const List = () => {
  const dispatch = useDispatch();
  const books = useSelector((state: StoreInterface) => state.books);
  const [disabled, setDisabled] = useState(false);
  const history = useHistory();

  const initialValues: SearchProps = { query: "" };

  useEffect(() => {
    let mounted = true;

    async function fetchAndSetBooks() {
      const fetchedBooks = await getAllBooks();
      if (mounted) dispatch(setBooksAction(fetchedBooks));
    }

    fetchAndSetBooks();

    return () => {
      mounted = false;
    };
  }, [dispatch]);

  return (
    <ListWrapper>
      <Button onClick={() => history.push("/books")}> NEW BOOK </Button>
      <Formik
        initialValues={initialValues}
        onSubmit={async (values) => {
          setDisabled(true);

          toast.info("Filtering...");

          const filteredBooks = await filterBooks(values.query);

          return sleep(2500).then(async () => {
            dispatch(setBooksAction(filteredBooks));
            setDisabled(false);
          });
        }}
      >
        <SearchForm>
          <FormInputWrapper>
            <FormInput
              backgroundColor="#F0F0F0"
              placeholder="Search Books"
              name="query"
              id="search-query"
            />
          </FormInputWrapper>
          <Button disabled={disabled}>SEARCH</Button>
        </SearchForm>
      </Formik>
      {books.map((book) => (
        <ListBookItem key={book.id} book={book} />
      ))}
    </ListWrapper>
  );
};
