import styled from "@xstyled/styled-components";
import { PageDefaultStyles } from "../../components/page-wrapper";

export const NotFoundWrapper = styled.div`
  ${PageDefaultStyles}
  height: 100vh;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const NotFoundText = styled.h1`
  font-size: 6em;
  font-weight: bold;
  margin-bottom: 5;
  color: violet-600;
`;
