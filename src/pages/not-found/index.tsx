import React from "react";
import { useHistory } from "react-router-dom";
import { Button } from "../../components/button";
import { PageWrapper } from "../../components/page-wrapper";
import { NotFoundText, NotFoundWrapper } from "./styles";

export const NotFound = () => {
  const history = useHistory();

  return (
    <PageWrapper>
      <NotFoundWrapper>
        <NotFoundText>404</NotFoundText>
        <Button onClick={() => history.push("/")}>BACK</Button>
      </NotFoundWrapper>
    </PageWrapper>
  );
};
