export interface LoginProps {
  name: string;
  password: string;
}

export interface BooksProps {
  title: string;
  description: string;
  id?: string;
  user?: string;
}
