import { BooksState } from "../../store/books/types";
import { serverUrl } from "../constants";
import { BooksProps } from "../types";

export const getAllBooks = async (): Promise<BooksState[]> => {
  const response = await fetch(`${serverUrl}/books`);
  return response.json();
};

export const filterBooks = async (query: string): Promise<BooksState[]> => {
  const response = await fetch(`${serverUrl}/books?q=${query}`);
  return response.json();
};

export const getBook = async (id: string | number): Promise<Response> => {
  const response = await fetch(`${serverUrl}/books/${id}`);
  return response;
};

export const addBook = async ({
  title,
  description,
}: BooksProps): Promise<BooksState[]> => {
  const response = await fetch(`${serverUrl}/books`, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ title, description }),
  });
  return response.json();
};

export const updateBook = async ({
  title,
  description,
  id,
  user,
}: BooksProps): Promise<BooksState> => {
  const response = await fetch(`${serverUrl}/books/${id}`, {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ title, description, user }),
  });
  return response.json();
};

export const deleteBook = async ({
  id,
}: {
  id: number | string;
}): Promise<BooksState[]> => {
  const response = await fetch(`${serverUrl}/books/${id}`, {
    method: "DELETE",
  });
  return response.json();
};
