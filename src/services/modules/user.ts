import { serverUrl } from "../constants";
import { LoginProps } from "../types";

interface User {
  id: string;
  name: string;
}

// Should be a post but i won't make a complete authentication/authorization feature :)

export const getUser = async ({
  name,
  password,
}: LoginProps): Promise<User[]> => {
  const response = await fetch(
    `${serverUrl}/users?name=${name}&password=${password}`
  );
  return response.json();
};
