import React from "react";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { BrowserRouter } from "react-router-dom";
import {
  defaultTheme,
  ThemeProvider,
  Preflight,
} from "@xstyled/styled-components";
import { ToastContainer } from "react-toastify";
import { injectStyle } from "react-toastify/dist/inject-style";
import { persistor, store } from "./store";
import GlobalStyle from "./styles/global";
import Routes from "./routes";
import "./index.css";

const theme = {
  ...defaultTheme,
  screens: {
    sm: 768,
    md: 968,
    lg: 1200,
  },
};

export const App = () => {
  injectStyle();

  return (
    <ThemeProvider theme={theme}>
      <Preflight />
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <BrowserRouter>
            <Routes />
            <GlobalStyle />
            <ToastContainer
              position="bottom-right"
              autoClose={2000}
              newestOnTop={false}
              pauseOnFocusLoss={false}
              pauseOnHover={false}
              rtl={false}
            />
          </BrowserRouter>
        </PersistGate>
      </Provider>
    </ThemeProvider>
  );
};
