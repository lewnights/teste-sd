import React from "react";
import { Switch } from "react-router-dom";
import Route from "./routes";
import { Login } from "../pages/login";
import { List } from "../pages/list";
import { BooksForm } from "../pages/books-form";
import { NotFound } from "../pages/not-found";

const Routes = () => {
  return (
    <Switch>
      <Route path="/" exact component={Login} isPrivate={false} />
      <Route path="/main" exact component={List} isPrivate />
      <Route path="/books/:id?" exact component={BooksForm} isPrivate />
      <Route path="*" component={NotFound} isPrivate />
    </Switch>
  );
};

export default Routes;
