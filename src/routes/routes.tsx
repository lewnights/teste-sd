import React from "react";
import { Redirect, Route, RouteProps } from "react-router-dom";
import { useSelector } from "react-redux";
import { StoreInterface } from "../store/store";
import { Header } from "../components/header/header";
import { PageWrapper } from "../components/page-wrapper";

interface Props extends RouteProps {
  component: React.ComponentType;
  isPrivate: boolean;
}

const RouteWrapper = ({ component: Component, isPrivate, ...rest }: Props) => {
  const signed = useSelector((state: StoreInterface) => state.auth.signed);

  if (!signed && isPrivate) {
    return <Redirect to="/" />;
  }

  if (signed && !isPrivate) {
    return <Redirect to="/main" />;
  }

  return (
    <Route
      {...rest}
      render={() =>
        signed ? (
          <>
            <Header />
            <PageWrapper>
              <Component />
            </PageWrapper>
          </>
        ) : (
          <Component />
        )
      }
    />
  );
};

export default RouteWrapper;
