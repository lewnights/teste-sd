import { createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension/developmentOnly";
import { persistStore } from "redux-persist";
import reducers from "./rootReducer";

const store = createStore(reducers, composeWithDevTools());

const persistor = persistStore(store);

export { store, persistor };
