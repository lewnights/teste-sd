import { AuthState } from "./auth/types";
import { BooksState } from "./books/types";

export interface StoreInterface {
  auth: AuthState;
  books: BooksState[];
}
