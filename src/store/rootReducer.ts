import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import { auth } from "./auth/reducer";
import { books } from "./books/reducer";

export default combineReducers({
  auth: persistReducer({ key: "books-key", storage }, auth),
  books,
});
