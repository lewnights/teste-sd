import { Reducer } from "redux";
import { AuthState } from "./types";

const INITIAL_STATE: AuthState = {
  id: "",
  name: "",
  signed: false,
};

export const auth: Reducer<AuthState> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SIGN_IN": {
      return {
        id: action.payload.id,
        name: action.payload.name,
        signed: true,
      };
    }
    case "SIGN_OUT": {
      return {
        ...INITIAL_STATE,
      };
    }
    default: {
      return state;
    }
  }
};
