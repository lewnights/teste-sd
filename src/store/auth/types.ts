export interface AuthState {
  id: string;
  name: string;
  signed?: boolean;
}
