import { AuthState } from "./types";

interface response {
  type: string;
  payload?: AuthState;
}

export function signIn(data: AuthState): response {
  return {
    type: "SIGN_IN",
    payload: data,
  };
}

export function signOut(): response {
  return {
    type: "SIGN_OUT",
  };
}
