import { Reducer } from "redux";
import { BooksState } from "./types";

const INITIAL_STATE: BooksState[] = [
  {
    id: "",
    title: "",
    description: "",
  },
];

export const books: Reducer<BooksState[]> = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case "SET_BOOKS": {
      return action.payload.map((book: BooksState) => ({
        id: book.id,
        title: book.title,
        description: book.description,
        user: book.user,
      }));
    }
    case "CLEAR_BOOKS": {
      return [...INITIAL_STATE];
    }
    case "REMOVE_BOOK": {
      return state.filter((book: BooksState) => {
        return book.id !== action.payload;
      });
    }
    case "UPDATE_BOOK": {
      return state.map((book: BooksState) => {
        if (book.id === action.payload.id) {
          return action.payload;
        }

        return book;
      });
    }
    default: {
      return state;
    }
  }
};
