import { BooksState } from "./types";

interface SetBookPayload {
  type: string;
  payload: BooksState[];
}

export function setBooksAction(data: BooksState[]): SetBookPayload {
  return {
    type: "SET_BOOKS",
    payload: data,
  };
}

interface UpdateBookPayload {
  type: string;
  payload: BooksState;
}

export function updateBookAction(data: BooksState): UpdateBookPayload {
  return {
    type: "UPDATE_BOOK",
    payload: data,
  };
}
interface ClearBookPayload {
  type: string;
}

export function clearBooksAction(): ClearBookPayload {
  return {
    type: "CLEAR_BOOKS",
  };
}

interface RemoveBookpayload {
  type: string;
  payload: string;
}

export function removeBookAction(id: string): RemoveBookpayload {
  return {
    type: "REMOVE_BOOK",
    payload: id,
  };
}
