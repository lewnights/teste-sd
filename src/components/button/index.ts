import styled from "@xstyled/styled-components";

interface ButtonProps extends React.HTMLAttributes<HTMLButtonElement> {
  margin?: string;
}

export const Button = styled.button<ButtonProps>`
  padding-top: 2;
  padding-bottom: 2;
  padding-right: 4;
  padding-left: 4;
  color: white;
  border-radius: md;
  font-weight: semibold;
  transition: default;
  background-color: true-gray-800;
  text-transform: uppercase;

  display: flex;
  align-items: center;

  &:hover {
    background-color: true-gray-900;
  }

  &:focus {
    outline: none;
    box-shadow: true-gray-ring;
  }

  margin: ${(p) => p.margin};
`;
