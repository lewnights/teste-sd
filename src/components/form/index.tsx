import styled from "@xstyled/styled-components";
import { Form, Field } from "formik";
import { Button } from "../button";

export const FormWrapper = styled.div`
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const CustomForm = styled(Form)`
  width: 99%;
  height: 500px;
  margin: 0 auto;
  background-color: violet-600;
  border-radius: 30px;

  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (min-width: sm) {
    width: 600px;
  }

  @media (min-width: lg) {
    width: 700px;
  }
`;

export const SearchForm = styled(Form)`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: warp;
  margin: 0 0 5 0;

  & > ${Button} {
    margin-top: 2;
  }

  @media (min-width: sm) {
    flex-direction: row;
    align-items: center;

    & > ${Button} {
      margin: 0;
    }
  }
`;

export const FormTitle = styled.h1`
  font-size: 2em;
  font-weight: bold;
`;

export const FormInputWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 80%;

  &:not(:first-of-type) {
    margin-top: 2;
  }
`;

export const FormLabel = styled.label`
  font-size: 1em;
  margin-bottom: 1;
  color: ${(p) => p.color || "white"};
`;

export const FormInput = styled(Field)`
  width: 100%;
  height: 40px;
  padding: 0 2;
  font-size: 0.8em;
  border-radius: 10px;
  color: black;
  background-color: ${(p) => p.backgroundColor || "white"};
`;

export const FormTextArea = styled(Field)`
  width: 100%;
  height: 140px;
  padding: 2;
  font-size: 0.8em;
  border-radius: 10px;
  color: black;
  resize: none;
`;

export const FormError = styled.p`
  color: white;
  font-size: 0.8em;
  margin: 2 0;
  font-weight: bold;
`;

export const FormButtonWrapper = styled.div`
  display: flex;
  align-items: center;
`;
