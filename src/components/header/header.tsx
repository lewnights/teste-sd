import React from "react";
import { useDispatch } from "react-redux";
import { signOut } from "../../store/auth/actions";
import { clearBooksAction } from "../../store/books/actions";
import { Button } from "../button";
import { HeaderWrapper, HeaderLogo, HeaderList, Logout } from "./styles";

export const Header = () => {
  const dispatch = useDispatch();

  const logout = () => {
    dispatch(signOut());
    dispatch(clearBooksAction());
  };

  return (
    <HeaderWrapper>
      <HeaderList>
        <HeaderLogo>BOOK STUFF!</HeaderLogo>
        <Button type="submit" onClick={logout}>
          LOGOUT <Logout />
        </Button>
      </HeaderList>
    </HeaderWrapper>
  );
};
