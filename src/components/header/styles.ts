import styled from "@xstyled/styled-components";
import { AiOutlineLogout } from "react-icons/ai";
import { PageDefaultStyles } from "../page-wrapper";

export const HeaderWrapper = styled.header`
  width: 100%;
  position: fixed;
  top: 0;
  left: 0;
  background-color: white;
  box-shadow: 0 3px 5px rgba(57, 63, 72, 0.3);
`;

// Pretend this is a logo please
export const HeaderLogo = styled.p`
  font-size: 1.2em;
  font-weight: bold;
  color: violet-600;
`;

export const HeaderList = styled.ul`
  ${PageDefaultStyles}
  display: flex;
  height: 60px;
  justify-content: space-between;
  align-items: center;
`;

export const Logout = styled(AiOutlineLogout)`
  color: white;
  margin-left: 1;
`;

export const HeaderListItem = styled.li``;
