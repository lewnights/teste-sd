import styled, { css } from "@xstyled/styled-components";

export const PageDefaultStyles = css`
  width: 90%;
  margin: 0 auto;

  @media (min-width: lg) {
    width: 1200px;
  }
`;

export const PageWrapper = styled.div`
  ${PageDefaultStyles}
  margin-top: 80px;
  min-height: calc(100vh - 80px);
`;
